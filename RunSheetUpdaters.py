import time
from config import params
import threading
from SheetUpdaters import SheetSyncCoordinator

do_threading=False

if __name__ == "__main__":        
    CAMPUS_OPTIONS, SOURCE_SHEET_NAME_KEY, SOURCE_GID_KEY, PULL_GID_KEY, PULL_SHEET_NAME_KEY, CREDENTIALS_FILE, UPDATER_KWARGS, campus_list, force_add_column_name = params()
    
    threads = []

    s=time.time()
    for i,campus in enumerate(campus_list):
        
        print('NOW UPDATING', campus,'time so far',time.time()-s)
        #here we actually call the sheet sync coordinator for each campus
        error_logs = []
        UPDATER_KWARGS['campus'] = campus #give the campus name to SheetSyncCoordinator for logging purposes
        g = SheetSyncCoordinator(
            SOURCE_SHEET_NAME_KEY[campus],
            SOURCE_GID_KEY[campus],
            PULL_SHEET_NAME_KEY[campus],
            PULL_GID_KEY[campus],
            False,
            cred_file=CREDENTIALS_FILE,
            ignore_row_one=True,
            force_add_column_name=force_add_column_name,
            updater_kwargs = UPDATER_KWARGS)
        if do_threading:
            if i%3==0 and i>0:
                time.sleep(65) #api wait
            x = threading.Thread(target=g.sync)
            threads.append((x,campus))
            x.start()
        else:
            if len(g.masterlist_df) == 0: #if there are no links in the pull sheet, quit
                continue
            error_logs += g.sync()

    if do_threading: #there's still no thread-safe logging, and the usage limits on the sheets api make threading at most marginally useful
        for (thread,campus) in threads:
            print('before joining',campus)
            thread.join()
            print('post joining',campus,'time so far',time.time()-s)

    if not error_logs: #empty list is falsy, non-empty list is truthy
        print()
        print("Finished with no errors!")
    else:
        print()
        print("Finished, with the following errors:")
        for error in error_logs:
            print(error.message)
