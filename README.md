# SRUBot

# INSTALLATION/USAGE
Run
> pip3 install -r requirements.txt

to install dependencies, then run as 

> python runsheetupdaters.py [options]

# Features/fixes new in current version:
* More detailed error handling with readable messages
* Error logging to file
* When bot encounters an error, it now unchecks the relevant row in the pull sheet and writes the error message to said row
* Bot now runs correctly on a sheet that's empty except for the column heads
* Bot now respects formulae and does not replace them with their static (evaluated) value
* Merged config files for all three units
* New -force_add command line flag that pushes a new column to all sheets in consideration (and populates them on the first run, as opposed to the old functionality that would require two bot runs)

# Known Limitations
* If formulae are filled in on otherwise blank rows, the bot reads these rows as not being blank and won't overwrite them
* debug.log only stores the most recent run to avoid accumulation of log files
* If distinct rows have the same Person ID, all but one of them will be ignored without any indication of that happening
* If the -force_add option is used, it may overwrite columns being used by organizers IF and ONLY IF these columns have either no name (in the top cell) or the name in the top cell occurs more than once among the columns of the sheet. Basically, name your columns if you want them to survive

# TODO 
- [ ] When we write to the pull sheet(s) using the gspread batch_update function (e.g to fill in the sheet key and GID, write the bot run date, error messages) we use hardcoded column positionals which make inserting new columns to the pull sheet(s) clunkier than it needs to be. Should be possible to make this writing based on the column name alone, and not its position in the sheet.
- [ ] Overhaul naming scheme within code (the interchangeable masterlist/five year list stuff is quite confusing, and five year list terminology is only applicable to certain units anyway)