import pandas as pd
import numpy as np
import gspread
import gspread_formatting as gformat
import logging

######### data utils #########

def clean_5yr_data(big_list):
    """takes: big_list, a 5 year list pandas df, cleans up person IDs, and 
    nans then returns it"""
    #some temporary fixes after five year list name changes
    if 'Person ID' in big_list.columns:
            big_list = big_list.rename(columns={'Person ID':'PersonID'})
    
    #remove duplicated rows, set personID to index for later merging purposes
    big_list = big_list.drop_duplicates(subset='PersonID') #TODO this is important
    big_list.loc[:,'PersonID'] = big_list['PersonID'].astype('int64') #TODO: this is throwing some weird pandas warning for 2865 but not SRU or 5810
    big_list = big_list.set_index('PersonID',verify_integrity=True)
    #fill nans with blanks, its nicer?
    #TODO: figure out dtypes and conflicts
    big_list = big_list.fillna('')
    return big_list      

######### misc sheet utils #########
def get_title_for_gid(match_gid, sh):
    all_wshs = sh.worksheets()
    title=None
    for a in all_wshs:
        if a.id == match_gid:
            title = a.title
    assert title is not None
    return title

def checkbox_to_bool(string):
    """the checkboxes in sheets are read in as strings reading TRUE or FALSE (in all caps as written) 
    which need to be converted to booleans"""
    assert string in ["TRUE", "FALSE", ''] #makes sure that the string in question is a valid checkbox
    if string in ['FALSE', '']: #we will treat missing checkboxes as unchecked
        return False
    elif string == "TRUE":
        return True
    
######### formatting utils #########
def highlight_rows(update_wsh, rows):
    if len(rows)==0:
        return
    fmt = gformat.cellFormat(
        backgroundColor=gformat.color(1, 1, 0), #set it to yellow
        textFormat=gformat.textFormat(foregroundColor=gformat.color(1, 0, 0)),
    )
    to_updt = [(str(row), fmt) for row in rows]
    gformat.format_cell_ranges(update_wsh, to_updt)
            
def green_header_names(update_wsh, g_cols, w_cols):
    """highlight the cols that match 5 year list data by turning them light green
    so it indicates which cols are being updated"""
    w_fmt = gformat.cellFormat(
            backgroundColor=gformat.color(1,1,1), #white
        )
    g_fmt = gformat.cellFormat(
            backgroundColor=gformat.color(152/256,251/256,152/256), #light green
        )
    #set one set green and one set white, batch all together for speed 
    to_updt_grn = [(gspread.utils.rowcol_to_a1(1, col), g_fmt) for col in g_cols]
    to_updt_w = [(gspread.utils.rowcol_to_a1(1, col), w_fmt) for col in w_cols]
    to_updt = to_updt_grn+to_updt_w
    gformat.format_cell_ranges(update_wsh, to_updt)
            

######### permissions utils #########

def remove_perms(update_sh, remove_emails=True):
    """given a gpsread sheet object, remove all permissions except the owner and sru-bot
    input: update_sh- gspread sheet object
    remove_emails - whether to remove individual people's permissions, or just link sharing"""
    all_perms = update_sh.list_permissions()
    for perm in all_perms:
        keys = perm.keys()
        if 'name' in keys:
            if ~remove_emails or perm['name'] == 'sru-bot@sruuaw.iam.gserviceaccount.com' or perm['role'] == 'owner':
                continue
            update_sh.remove_permissions(perm['emailAddress'])
        elif perm['id'] == 'anyoneWithLink':
            update_sh.remove_permissions(perm['id'])
        else:
            print('couldnt deal with', perm)
            
def get_perm_email_list(update_sh):
    """given a gspread sheet object, create a str
    input: update_sh- gspread sheet object
    output: email_list - list of all emails with permission for the document"""
    all_perms = update_sh.list_permissions()
    email_list = []
    for perm in all_perms:
        keys = perm.keys()
        if 'name' in keys:
            #don't list the bot
            if perm['name'] == 'sru-bot@sruuaw.iam.gserviceaccount.com': 
                continue
            if 'emailAddress' in keys:
                email_list.append(perm['emailAddress'])
            else:
                #TODO: make this log a warning to a file
                print('couldnt deal with', perm)
        elif 'emailAddress' in keys: #TODO: why do some have names and some emails?
            email_list.append(perm['emailAddress'])
            
        elif perm['id'] == 'anyoneWithLink':
            #TODO: for now just ignore this in this fxn
            continue
        else:
            #TODO: make this log a warning to a file
            print('couldnt deal with', perm)
    return email_list


def add_perms(update_sh, perms_add, role='writer'):
    """add role permissions for sheet for all in perms_add list
    Inputs: update_sh-gspread sheet object
    perms_add - list of emails
    role (str) – (optional) role of users to add. Allowed values are: owner, writer, reader. Default to writer
    """
    for perm in perms_add:
        if perm!='':
            #TODO: check if already permitted
            update_sh.share(perm, perm_type='user', role=role, notify=False)

class CustomStreamHandler(logging.StreamHandler): #custom logging.Handler to store error logs for printing at the end
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.error_logs = []

    def emit(self, record):
        if record.levelno == logging.ERROR:
            self.error_logs.append(record)
        super().emit(record)