from datetime import date, datetime
import dateutil.parser as dateparser
import pytz
import time
import pandas as pd
import numpy as np
import gspread
import gspread_formatting as gformat
from sheet_update_utils import *
import logging
import requests #just for error handling here
import traceback

### Logging config, CustomStreamHandler stores error logs in a list so you can view them all together at the end
### Credit to RafalS on stackoverflow
handler = CustomStreamHandler()
handler.setLevel(logging.INFO) #only info or higher level logs are printed
logging.basicConfig(level=logging.DEBUG, #debug and higher level logs are logged to debug.log, which is quite verbose
                format="%(asctime)s [%(levelname)s] %(message)s", 
                handlers=[handler, logging.FileHandler(filename="debug.log", mode='w')])
                #FileHandler(filename="logs/"+ time.ctime().replace(' ', '_') + "_debug.log", mode='w')]) #TODO use if you want to store all logs, not just rewrite over the same file

pd.options.mode.chained_assignment = None  #suppresses a deprecation warning, possibly unwise

### GLOSSARY OF TERMS
### Five year list and variations thereof (in, for example, variable names): the source data sheet with all of the info on your in-unit workers
### Masterlist: the sheet containing the list of sheets to be updated by the bot (also referred to as the pull sheet)

class SheetSyncCoordinator():
    def __init__(self, five_yr_name, five_yr_gid, masterlist_name, masterlist_gid, remove_emails, cred_file=None,
     ignore_row_one=True, force_add_column_name='', updater_kwargs = {}):
        if cred_file is None:
            self.gc = gspread.service_account()
        else:
            self.gc = gspread.service_account(cred_file)
        self.time_format = '%a %m/%d/%y %I:%M %p'
        self.remove_emails = remove_emails
        #gets the full source data file as a dataframe, and the date said data was last updated (by convention stored in cell A1 of the source data sheet)
        self.five_yr_df, self.five_yr_update_date = self.get_five_yr(five_yr_name, five_yr_gid, ignore_row_one)
        #the spreadsheet with all dept sheet links+names for the different campuses, also called the pull sheet
        self.masterlist_wsh, self.masterlist_df = self.get_masterlist(masterlist_name, masterlist_gid)
        if len(self.masterlist_df) > 0: #if len masterlist_df is 0, then there are no links, and RunSheetUpdaters will break. not filling keys here is just to avoid an error in that function for a pull sheet with no links
            self.fill_keys_masterlist() #in the pull sheet, extracts and writes the sheet key and GID from the link #TODO this writes every time, waste of API limits
        self.updater_kwargs = updater_kwargs
        self.force_add_column_name = force_add_column_name #supplied at runtime, command line option. if supplied, force adds this column to all sheets in question
        self.campus = self.updater_kwargs.pop('campus').upper() #pop the campus name so that it isn't passed to anything else that needs the updater_kwargs

        ### Unpack the updater_kwargs so that they can be passed to the DeptSheetUpdater
        ### Previously we were just passing **self.updater_kwargs to the DeptSheetUpdater but this makes it difficult to set (say) the remove_not_in_five_yr boolean in multiple places
        ### E.g, it is possible to set a default value for this flag in config.py for each unit, and set it manually for each sheet in the pull sheet via a checkbox
        if 'remove_not_in_five_yr' in self.updater_kwargs:
            self.remove_not_in_five_yr = self.updater_kwargs['remove_not_in_five_yr'] #if there is a default value in the config file, set it
        else:
            self.remove_not_in_five_yr = False #if not, default False (but the checkbox in the pull sheet will override this value)
        if 'highlight_new' in self.updater_kwargs:
            self.highlight_new = self.updater_kwargs['highlight_new'] #same as above
        else:
            self.highlight_new = False

    def get_five_yr(self, five_yr_name, five_yr_gid, ignore_row_one):
        """Gets the five-year list as a pd dataframe, cleans it, and returns it.
        Uses a helper function to get and format the date the five-year list was updated, if available."""

        # Retry logic for opening the sheet and worksheet
        big_list_sh, big_list_worksheet = None, None
        for k in range(1, 6 + 1):  # Retry up to 6 times
            try:
                big_list_sh, big_list_worksheet = self.open_sheet_and_wsh(five_yr_name, wsh_gid=five_yr_gid)
                break  # Exit loop if successful
            except gspread.exceptions.APIError as e:
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning(f"502 Error on API call when opening sheet, retrying. Attempt {k}")
                    time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                else:
                    logging.error(f"Unexpected API error: {str(e)}")
                    raise e  # If not a 502 error, raise it
        else:
            logging.error(f"Failed to open sheet after {k} retries.")
            raise gspread.exceptions.APIError("Failed to open sheet after retries.")

        # Determine where to start reading records based on ignore_row_one
        head = 1
        if ignore_row_one:
            head = 2

        # Extracts the date data was last refreshed from cell A1
        update_date = self.get_five_yr_update_date(big_list_worksheet, ignore_row_one)

        # Retry logic for retrieving all records from the worksheet
        for k in range(1, 6 + 1):  # Retry up to 6 times
            try:
                big_list = pd.DataFrame(big_list_worksheet.get_all_records(head=head))
                break  # Exit loop if successful
            except gspread.exceptions.APIError as e:
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning(f"502 Error on API call when getting records, retrying. Attempt {k}")
                    time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                else:
                    logging.error(f"Unexpected API error: {str(e)}")
                    raise e  # If not a 502 error, raise it
        else:
            logging.error(f"Failed to retrieve records after {k} retries.")
            raise gspread.exceptions.APIError("Failed to retrieve records after retries.")

        # Ensure all values are cast to string
        big_list = big_list.astype(str)

        # Attempt to clean the five-year data
        try:
            big_list = clean_5yr_data(big_list)
        except KeyError:
            logging.error('Source five-year list/data sheet does not contain PID column. Quitting now.')
            raise SystemExit  # Exits the program

        return big_list, update_date

    def get_five_yr_update_date(self, five_yr_worksheet, ignore_row_one):
        """helper fxn for get five year to extract date updated"""
        if not ignore_row_one:
            update_date = "not available"
        else:
            update_date_orig_str = five_yr_worksheet.cell(1,1).value #not 0 index
            try:
                stripped_str = update_date_orig_str.split(',')
            except AttributeError:
                logging.error("Source sheet for " + self.campus + " does not have an update date in cell A1, likely because there are not enough rows in the given source sheet. For now we have to manually add rows and re-run the script that updates the source sheet.")
            if len(stripped_str) < 3: #date not in expected format 
                update_date = update_date_orig_str #just take orig
            else: #reformat to my format
                update_date = dateparser.parse(stripped_str[1]+' '+stripped_str[2])
                update_date = update_date.strftime(self.time_format).replace(" 0", " ")
        return update_date

    def get_masterlist(self, masterlist_name, masterlist_gid):
        """Gets the sheet of all updates to make, with retry logic for handling API errors."""

        # Retry logic for opening the sheet and worksheet
        sh, master_wsh = None, None
        for k in range(1, 6 + 1):  # Retry up to 6 times
            try:
                sh, master_wsh = self.open_sheet_and_wsh(masterlist_name, wsh_gid=masterlist_gid)
                break  # Exit the retry loop if successful
            except gspread.exceptions.APIError as e:
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning(f"502 Error on API call when opening masterlist, retrying. Attempt {k}")
                    time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                else:
                    logging.error(f"Unexpected API error when opening masterlist: {str(e)}")
                    raise e
        else:
            logging.error(f"Failed to open masterlist after {k} retries.")
            raise gspread.exceptions.APIError("Failed to open masterlist after retries.")

        # Retry logic for fetching all records from the masterlist
        for k in range(1, 6 + 1):  # Retry up to 6 times
            try:
                masterlist_df = pd.DataFrame(master_wsh.get_all_records())  # Get all records and convert to DataFrame
                break  # Exit the retry loop if successful
            except gspread.exceptions.APIError as e:
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning(f"502 Error on API call when fetching masterlist records, retrying. Attempt {k}")
                    time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                else:
                    logging.error(f"Unexpected API error when fetching masterlist records: {str(e)}")
                    raise e
        else:
            logging.error(f"Failed to fetch masterlist records after {k} retries.")
            raise gspread.exceptions.APIError("Failed to fetch masterlist records after retries.")

        return master_wsh, masterlist_df

    def fill_keys_masterlist(self):
        """After opening the masterlist, fill in key and GID info from link info.
        Long-term, this should not need to be written back out to save API calls, but for now, it does."""

        # Check for errors in the links
        for i, row in self.masterlist_df.iterrows():
            try:
                assert '/d/' in row.link
                assert 'gid=' in row.link
            except AssertionError as e:
                raise Exception("Link invalid for row " + str(i) + " which is sheet " + row.sheet_name)

        # Extract the key and worksheet ID from the link
        self.masterlist_df['key'] = self.masterlist_df.link.apply(lambda st: st.split('/d/')[1].split('/')[0])
        # Isolate the GID, future-proofing for any additional query elements that appear after the GID
        self.masterlist_df['wsheet_id'] = self.masterlist_df.link.apply(lambda st: st.split('gid=')[-1].split('&')[0].split('#')[0])

        # Prepare the batch update to write the extracted key and GID back to the Google Sheet
        to_updt = [
            {'range': 'F2:F' + str(len(self.masterlist_df) + 1),
             'values': [[a] for a in self.masterlist_df.key.values.tolist()]},
            {'range': 'G2:G' + str(len(self.masterlist_df) + 1),
             'values': [[a] for a in self.masterlist_df.wsheet_id.values.tolist()]}
        ]

        # Retry logic for the batch update to handle 502 errors
        for k in range(1, 6 + 1):  # Retry up to 6 times
            try:
                self.masterlist_wsh.batch_update(to_updt)
                break  # Exit the retry loop if successful
            except gspread.exceptions.APIError as e:
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning(f"502 Error on API call when batch updating masterlist, retrying. Attempt {k}")
                    time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                else:
                    logging.error(f"Unexpected API error: {str(e)}")
                    raise e  # If not a 502 error, raise it
        else:
            logging.error(f"Failed to batch update masterlist after {k} retries.")
            raise gspread.exceptions.APIError("Failed to batch update masterlist after retries.")

        # Convert the wsheet_id to int32 for later search functionality
        self.masterlist_df['wsheet_id'] = self.masterlist_df['wsheet_id'].astype('int32')

    def sync(self):
        for i, row in self.masterlist_df.iterrows():
            masterlist_row = i + 2  # Add 2 for header and non-zero index
            try:
                if row.include in ['FALSE', '']:  # Ignore row if box is unchecked or not present
                    if row.include == '':
                        logging.warning(
                            'Warning: ' + self.campus + ' sheet ' + row.sheet_name + ' does not have a checkbox in the pull sheet, will not update')
                    continue
                logging.info('Updating sheet ' + row.sheet_name)
                s = time.time()

                update_sh, update_wsh = self.open_sheet_and_wsh(row.key, wsh_gid=row.wsheet_id,
                                                                key_open=True)  # Open for validation
                logging.debug('Time to get sheet: ' + str(time.time() - s))
                self.remove_not_in_five_yr = checkbox_to_bool(
                    row.remove_not_in_masterlist)  # Set variable from checkbox value
                dupt = DeptSheetUpdater(self.five_yr_df, row, update_sh, update_wsh, self.force_add_column_name,
                                        self.highlight_new, self.remove_not_in_five_yr)
                dupt.update_data()

                # Fix perms + return list of current email
                if self.remove_emails:
                    email_list = dupt.update_perms(self.remove_emails)
                else:
                    email_list = ''

                # Update masterlist with permissions data, five-year update date, and bot run date
                to_updt = [{'range': 'D' + str(masterlist_row), 'values': [[email_list]]},
                           {'range': 'H' + str(masterlist_row), 'values': [[self.five_yr_update_date]]},
                           {'range': 'I' + str(masterlist_row), 'values': [[datetime.now(
                               pytz.timezone('America/Los_Angeles')).strftime(self.time_format).replace(" 0", " ")]]}]
                self.masterlist_wsh.batch_update(to_updt)
                logging.debug('Sheet updated in ' + str(time.time() - s))
                del dupt  # Save for cloud app

            except Exception as e:  # Catch all errors
                error_type = type(e).__module__ + '.' + type(e).__name__  # Get full error type as string
                error_string = str(e)  # Convert the error to string for logging

                if error_type == 'gspread.exceptions.APIError' or error_type == 'requests.exceptions.ConnectionError':
                    if error_type == 'gspread.exceptions.APIError':
                        # Check if the response looks like HTML (which indicates a server error)
                        if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                            logging.warning("Received HTML error response from API, indicating a server issue.")
                            error_string = "Received a 502 (Bad Gateway) error from Google Sheets API. Please try again later."
                        elif isinstance(e.args[0], dict) and 'code' in e.args[0]:
                            if e.args[0]['code'] == 403:  # Permission Error
                                error_string = "Bot does not have permission to access this sheet"
                            elif e.args[0]['code'] == 404:  # Sheet not found error
                                error_string = "Sheet not found, link may be invalid"
                        else:
                            logging.warning('Invalid error structure received from the API: {}'.format(e))
                    else:
                        # Handle other connection errors and retry logic
                        logging.warning('ERROR IN GSPREAD API OPENING WSH, WAITING AND RETRYING')
                        error_string = "Issue with Sheets API, either idiopathic or due to excessive requests"
                        for k in range(1, 6 + 1):
                            try:
                                time.sleep(2 ** k)  # Exponential backoff
                                wsh_title = get_title_for_gid(wsh_gid, sheet)
                                worksheet = sheet.worksheet(wsh_title)
                                logging.warning('SUCCESS reopening sheet after wait')
                                error_string = ''  # Clear error string if successful
                                break  # Exit loop if it works
                            except Exception as retry_error:
                                logging.warning(f"Retrying, waiting {2 ** k} seconds. Retry error: {retry_error}")
                                continue

                elif error_type == 'gspread.exceptions.WorksheetNotFound':
                    error_string = 'worksheet ' + str(
                        wsh_gid) + ' not found in sheet ' + sheet_name + ', available worksheets are: ' + ', '.join(
                        sheet.worksheets())

                # Handle errors and log to masterlist sheet
                if error_string:
                    # Highlight the row of the sheet that failed to update in red
                    gformat.format_cell_range(self.masterlist_wsh, gspread.utils.rowcol_to_a1(masterlist_row,
                                                                                              1) + ':' + gspread.utils.rowcol_to_a1(
                        masterlist_row, self.masterlist_wsh.col_count),
                                              gformat.cellFormat(backgroundColor=gformat.color(1, 0, 0)))
                    # Uncheck include box, mark row red, and write brief description of error to master sheet
                    self.masterlist_wsh.batch_update([{'range': 'J' + str(masterlist_row), 'values': [[False]]},
                                                      {'range': 'L' + str(masterlist_row), 'values': [[error_string]]}])
                    logging.error(
                        self.campus + ' sheet ' + row.sheet_name + ' failed to update with error message: ' + error_string)
                    logging.debug(traceback.format_exc())  # Log full traceback for debugging

                continue  # Continue to next row

        logging.info('Finished updating ' + self.campus + '!')
        return handler.error_logs  # Return error logs to be saved and printed at the end

    def open_sheet_and_wsh(self, sheet_name, wsh_gid=None, key_open=False):
        """open and return sheet and specific worksheet, handling errors (ish).
        Will open worksheet named "Sheet1" unless gid is provided.
        Assumes that sheet_name is actual name, but if key_open is True will open by key"""
        if key_open: #if key open, use sheet key to open the sheet
            sheet = self.gc.open_by_key(sheet_name)
        else:
            sheet = self.gc.open(sheet_name)

        if (wsh_gid is not None) and (wsh_gid != ''): 
            wsh_title = get_title_for_gid(wsh_gid, sheet)
        else: 
            wsh_title = 'Sheet1' #DEPRECATED: use default 'Sheet1' title
        worksheet = sheet.worksheet(wsh_title)

        return sheet, worksheet

class DeptSheetUpdater:
    """Handles the updating of a single dept sheet"""
    def __init__(self, five_yr_df, update_row, update_sh, update_wsheet, force_add_column_name='', highlight_new=False,
        remove_not_in_five_yr=False):
        self.five_yr_df = five_yr_df 
        self.row = update_row
        self.update_sh = update_sh
        self.update_wsheet = update_wsheet
        self.orig_col_order = []
        self.force_add_column_name = force_add_column_name
        self.highlight_new = highlight_new
        self.remove_not_in_five_yr = remove_not_in_five_yr

        ### self.new_sheet is a class variable that is set to true if bot detects a sheet with only the header row
        ### in this case, the writing procedure is slightly altered to avoid the bug where we used to have to add garbage data to row 2 for the bot to write the first time it runs on a given sheet
        self.new_sheet = False #initialized as False


    def update_perms(self, remove_emails):
        """call all utils for permission maintenance
        calls_remove perms with email options
        gets list of permissions to add from sheet + adds them
        gets a list of all permissions shared, return list"""
        
        if remove_emails: #if have removed email perms, re-add the ones off the sheet
            #TODO: remove perms wasn't disabling link sharing, so for now only run if remove emails
            #butt long term should be fixed?
            remove_perms(self.update_sh, remove_emails=remove_emails) 
            perms_list = self.row.permitted
            perms_add = perms_list.split(', ')
            add_perms(self.update_sh, perms_add)
        #then get all perms in list form, and write to the masterlist
        email_list = get_perm_email_list(self.update_sh)
        return ', '.join(email_list)

    def update_data(self):
        try:
            data_to_update = self.get_data_to_update()
            data_to_update = self.fix_PIDs(data_to_update)
            original_num_rows = len(data_to_update)

            updated_data = self.update_df(data_to_update)
            updated_data, new_indices = self.get_missing_people(updated_data)

            if self.remove_not_in_five_yr:
                # worksheet.clear() is buggy (clears data validation / checkboxes)
                # Instead, add enough blank rows to the DataFrame to ensure
                # that the updated data can completely overwrite the existing data.
                num_rows_after_updates = len(updated_data)
                for row_num in range(num_rows_after_updates, original_num_rows):
                    updated_data.loc[row_num] = ''



            # Push the updates to the sheet
            self.update_wsheet.update([updated_data.columns.values.tolist()] + updated_data.values.tolist(), raw=False)

            # Highlight newly added people if required
            if self.highlight_new:
                highlight_rows(self.update_wsheet, [i + 2 for i in new_indices])  # +2 for 0-index and header

            self.highlight_updated_cols()

        except gspread.exceptions.APIError as e:
            error_type = type(e).__module__ + '.' + type(e).__name__
            if error_type == 'gspread.exceptions.APIError':
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning("Received HTML error response from API, indicating a server issue.")
                    # Retry logic with exponential backoff for server-related errors
                    for k in range(1, 6 + 1):  # Retry up to 6 times
                        try:
                            time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                            data_to_update = self.get_data_to_update()  # Retry getting the data
                            logging.warning('SUCCESS after retrying, data retrieved after wait')
                            break  # Exit the retry loop if successful
                        except Exception as retry_error:
                            logging.warning(f"Retrying, waiting {2 ** k} seconds. Retry error: {retry_error}")
                            continue
                    else:
                        logging.error(f"Failed after multiple retries for API error: {e}")
                        raise e  # Re-raise if all retries fail
                else:
                    logging.error(f"Unexpected API error structure: {str(e)}")
                    raise e  # Re-raise for unexpected errors
            else:
                logging.error(f"Unexpected error: {str(e)}")
                raise e  # Re-raise for any other unexpected errors

    def get_data_to_update(self):
        """Takes in data, converts to a dataframe, and correctly orders columns"""
        try:
            records = self.update_wsheet.get_all_records(value_render_option="Formula", expected_headers={})

            # Handle case for blank sheets
            if records == []:
                self.new_sheet = True  # Flag to modify how blank sheets are written
                records = [{x: '' for x in self.update_wsheet.get_values(value_render_option="Formula")[
                    0]}]  # Initialize empty columns

            # Get original column order to maintain it
            self.orig_col_order = records[0].keys()

            # Remove columns with blank headers
            self.orig_col_order = [c for c in self.orig_col_order if c != '']

            # Create a dataframe with the records
            data_to_update = pd.DataFrame(records)
            data_to_update = data_to_update[self.orig_col_order]

            # Rename 'Person ID' column to 'PersonID'
            if 'Person ID' in data_to_update.columns:
                data_to_update = data_to_update.rename(columns={'Person ID': 'PersonID'})
                self.orig_col_order = [c if c != 'Person ID' else 'PersonID' for c in self.orig_col_order]

            # Ensure 'PersonID' column is present
            assert 'PersonID' in data_to_update.columns, "Missing 'PersonID' column in data"

            # Ensure there are no duplicated columns
            assert not np.any(
                data_to_update.columns.duplicated()), f"Duplicated columns found: {', '.join(data_to_update.columns[data_to_update.columns.duplicated()])}"

            return data_to_update

        except gspread.exceptions.APIError as e:
            error_type = type(e).__module__ + '.' + type(e).__name__

            if error_type == 'gspread.exceptions.APIError':
                if isinstance(e.args[0], str) and e.args[0].startswith("<!DOCTYPE html>"):
                    logging.warning("Received HTML error response from API, indicating a server issue.")
                    # Retry logic with exponential backoff
                    for k in range(1, 6 + 1):  # Retry up to 6 times
                        try:
                            time.sleep(2 ** k)  # Exponential backoff: 2, 4, 8, 16, 32, 64 seconds
                            records = self.update_wsheet.get_all_records(value_render_option="Formula",
                                                                         expected_headers={})
                            logging.warning('SUCCESS after retrying, records retrieved after wait')
                            return records  # Exit the retry loop if successful
                        except Exception as retry_error:
                            logging.warning(f"Retrying, waiting {2 ** k} seconds. Retry error: {retry_error}")
                            continue
                    raise e  # If all retries fail, raise the original error
                else:
                    logging.error(f"Invalid error structure: {str(e)}")
                    raise
            else:
                logging.error(f"Unexpected error: {str(e)}")
                raise

    def fix_PIDs(self, data_to_update):
        """fixes person ID column to make them be a unique identifier"""
        data_to_update['PersonID'] = pd.to_numeric(data_to_update['PersonID']) #conv to #
        data_to_update['PersonID'] = data_to_update['PersonID'].fillna(0, downcast='infer').astype('int64') #fillnan
        data_to_update.replace([np.inf, -np.inf], np.nan, inplace=True)  # Replace inf/-inf with NaN
        data_to_update.fillna(0, inplace=True)  # Replace NaN with 0 or another placeholder

        try:
            data_to_update = data_to_update.set_index('PersonID', verify_integrity=True)
        except:
            #take the min if there's other fakes (negs) or start at -1
            if data_to_update.PersonID.min() < 0:
                counter = data_to_update.PersonID.min()-1
            else:
                counter = -1
            for dup_row in data_to_update.index.values[
                    np.where(data_to_update.duplicated(subset='PersonID'))[0]]:
                data_to_update.loc[dup_row,'PersonID'] = counter
                logging.debug('filling in some PIDs')
                counter-=1
            #set the index to be person ID for the purposes of merging
            data_to_update = data_to_update.set_index('PersonID', verify_integrity=True)  
        return data_to_update

    def update_df(self, data_to_update):
        """update the wsheet data with the five year list info, then return
        it to correct format+ mark any rows unable to be updated"""
        data_to_update.update(self.five_yr_df)


        if self.remove_not_in_five_yr:
            data_to_update = self.delete_not_in_5yr(data_to_update)
        data_to_update = self.mark_not_in_5yr(data_to_update) #this is required to add bot notes

        #set person id back to col, not index
        data_to_update.reset_index(level='PersonID', inplace=True)
        
        col_order = self.orig_col_order
        if 'BotNotes' not in col_order:
            logging.info(self.row.sheet_name + ' newly added bot notes column')
            col_order = col_order + ['BotNotes']

        if self.force_add_column_name: #nonnempty strings are truthy
            data_to_update, col_order = self.add_column(self.force_add_column_name, data_to_update, col_order) #push column

        data_to_update = data_to_update[col_order]
        if self.new_sheet == True: #we created a dummy first (really, second) row in the data of our brand new sheet, which we now remove
            data_to_update.drop(0, inplace=True)

        return data_to_update
    
    def mark_not_in_5yr(self, data_to_update):
        """clear prev PID not in five year info in botnotes, find rows with PIDs not in five year,
        and write that they weren't found"""
        missing = np.setdiff1d(data_to_update.index, self.five_yr_df.index)
        if 'BotNotes' in data_to_update.columns:
            #erase previously noted missings as they may have  been corrected now
            data_to_update.loc[data_to_update.BotNotes=='PID Not In List','BotNotes'] = ''
        data_to_update.loc[data_to_update.index.isin(missing),'BotNotes'] = 'PID Not In List'
        data_to_update['BotNotes'] = data_to_update['BotNotes'].fillna('')
        return data_to_update
        
    def delete_not_in_5yr(self, data_to_update):
        """find rows with PIDs not in five year, and delete them"""
        missing = np.setdiff1d(data_to_update.index, self.five_yr_df.index)
        data_to_update = data_to_update.drop(missing)
        return data_to_update


    def get_missing_people(self, data_to_update):
        """look through five year list for people matching filt_vals (based on col_for_filt)
        and then see if any aren't in the dept sheet based on PID. If so, add them.
        Return the dataframe with new people added, and a list of indices of added people"""
        cols_vals = self.row.col_for_filt.split('; ')
        f_vals = str(self.row.filt_val).split('; ')
        logging.debug('The columns to filter for are:' + ', '.join(cols_vals))

        relevant_people = pd.DataFrame()

        for col_to_filt in cols_vals:
            matches = self.five_yr_df.loc[self.five_yr_df[col_to_filt].astype(str).isin(f_vals), :]
            relevant_people = pd.concat([relevant_people, matches])
            #search using wildcards that follow regex conventions
            #TODO: is there any reason to not just use match for all filt vals?
            for f in f_vals:
                if ('+' in f) or ('*' in f):
                    wildcard_results = self.five_yr_df.loc[
                        self.five_yr_df[col_to_filt].str.match(f), :]
                    #print(f, 'Wild card filter produced:', len(wildcard_results), 'results')
                    relevant_people = pd.concat([relevant_people, wildcard_results])

        missing_PIDs = np.setdiff1d(relevant_people.index, data_to_update.PersonID)
        missing_people = relevant_people.loc[relevant_people.index.isin(missing_PIDs),:]
        missing_people = missing_people.reset_index(level='PersonID')

        col_order = data_to_update.columns #this is in case column order changes in update_df if we force add a column or add BotNotes
        try:
            merged = pd.concat([data_to_update, missing_people], ignore_index=True, sort=False)
        except Exception as e:
            logging.error('ENCOUNTERED A PROBLEM MERGING NEW PEOPLE')
            #logging.error('missing people', missing_people.head())
            #logging.error('data_to_update', data_to_update.head())
            #logging.error('big list cols', missing_people.columns, 'update cols', data_to_update.columns)
            raise e
        #only take previously extant cols
        merged = merged[col_order]
        #fill nans for the added rows only bc other nans shouldnt exist #TODO this is how i can fix formulae
        merged.loc[merged.PersonID.isin(missing_PIDs),:] = (
            merged.loc[merged.PersonID.isin(missing_PIDs),:].fillna(''))
        #make note in botnotes column of added person
        merged.loc[merged.PersonID.isin(missing_PIDs),'BotNotes'] = 'Missing, added by bot ' + \
               date.today().strftime("%m/%d/%Y")
        #get indices of missing people
        indices_missing = merged.index[merged.PersonID.isin(missing_PIDs)]
        return merged, indices_missing
    

    def highlight_updated_cols(self):
        """gets indices of which cols updated by bot, then highlights them green"""
        #all cols in five year are updated by bot, so get those, and i+1 for zero idx
        col_indices_updated = [i+1 for i,c in enumerate(self.orig_col_order)
                            if c in self.five_yr_df.columns]
        #get col not updated to set them white
        col_indices_not_updated = [i+1 for i,c in enumerate(self.orig_col_order)
                            if c not in self.five_yr_df.columns]
        #make columns that are being updated green
        green_header_names(self.update_wsheet, col_indices_updated, col_indices_not_updated)

    def add_column(self, column_name, data_to_update, col_order):
        """adds column at end and adds it to the appropriate dataframes so that it gets written to
        if it corresponds to a column in the 5yr list"""
        if column_name in col_order: #refuses to add column if it already exists
            logging.warning("Disable add_column function, column " + column_name + " already exists")
            return data_to_update, col_order
        else:
            data_to_update.set_index('PersonID', inplace=True)
            data_to_update[column_name] = None #initialize new blank column
            col_order += [column_name]
            data_to_update.update(self.five_yr_df) #update the newly initialized column with the data from the five year list (if it corresponds to a column in the five year list)
            data_to_update.reset_index(level='PersonID', inplace=True) #maybe a cleaner way to do this, but have to let PersonID be the index for the .update function inn add_column to fill the new column in on the first run, then reset it to just another column TODO
            data_to_update = data_to_update[col_order]
            logging.info('Newly added ' + column_name + ' column')
            return data_to_update, col_order
 